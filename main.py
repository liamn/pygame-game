import pygame
import math
import random

import spritesheet

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
 
# Screen dimensions
SCREEN_WIDTH = 960
SCREEN_HEIGHT = 540

ScrollX = 0
ScrollY = 0

Y_OFF = [(6, -17), (11, -16), (18, -15), (24, -12), (27, -8), (30, -5), (31, -2), (0, -18)]
X_OFF = [(31, 2), (30, 5), (27, 8), (24, 12), (18, 15), (11, 16), (6, 17), (32, 0)]

class Entity:
    def __init__(self, x, y):
        self.gridx = x
        self.gridy = y

    def update(self, frame, r, ScrollX, ScrollY):
        self.frame = math.floor((r % 90)/11.25) - 1
        quadrant = (r % 360)//90
        f = self.frame
        
        self.gridx2 = self.gridx + ScrollX % 1
        self.gridy2 = self.gridy + ScrollY % 1

        if quadrant == 0:
            self.newx, self.newy = self.gridx2, self.gridy2
        elif quadrant == 1:
            self.newx, self.newy = self.gridy2, -self.gridx2
        elif quadrant == 2:
            self.newx, self.newy = -self.gridx2, -self.gridy2
        elif quadrant == 3:
            self.newx, self.newy = -self.gridy2, self.gridx2

        self.x = self.newx * X_OFF[f][0] * 2 + self.newy * Y_OFF[f][0] * 2
        self.y = self.newx * X_OFF[f][1] * 2 + self.newy * Y_OFF[f][1] * 2


        self.x += SCREEN_WIDTH / 2
        self.y += SCREEN_HEIGHT / 2

    def round(self, amount): 
        self.x = (self.x // amount) * amount
        self.y = (self.y // amount) * amount



class Tile:
    def __init__(self, x, y):
        self.gridx = x
        self.gridy = y
        self.update(0, 0, 0, 0)

    def update(self, frame, r, ScrollX, ScrollY):
        self.frame = math.floor((r % 90)/11.25) - 1
        quadrant = (r % 360)//90
        f = self.frame
        self.gridx2 = self.gridx + ScrollX % 1
        self.gridy2 = self.gridy + ScrollY % 1

        if quadrant == 0:
            self.newx, self.newy = self.gridx2, self.gridy2
        elif quadrant == 1:
            self.newx, self.newy = self.gridy2, -self.gridx2
        elif quadrant == 2:
            self.newx, self.newy = -self.gridx2, -self.gridy2
        elif quadrant == 3:
            self.newx, self.newy = -self.gridy2, self.gridx2

        self.x = self.newx * X_OFF[f][0] * 2 + self.newy * Y_OFF[f][0] * 2
        self.y = self.newx * X_OFF[f][1] * 2 + self.newy * Y_OFF[f][1] * 2

        self.x += SCREEN_WIDTH / 2
        self.y += SCREEN_HEIGHT / 2


    def round(self, amount): 
        self.x = (self.x // amount) * amount
        self.y = (self.y // amount) * amount
 
 
def main():
    """ Main Program """
    pygame.init()
 
    # Set the height and width of the screen
    size = [SCREEN_WIDTH, SCREEN_HEIGHT]
    screen = pygame.display.set_mode(size)
    frame = 0

    ScrollX = 0.0
    ScrollY = 0.0
    ScrollXSpeed = 0
    ScrollYSpeed = 0
    SCROLLSPEED = 0.125

    pygame.display.set_caption("simple tile map")
  
    movement = 1
    movement_speed = 90 / 64
    textures = []
    textures += [spritesheet.spritesheet('grass.png')]
    textures += [spritesheet.spritesheet('dirt.png')]
    
    playerimage = pygame.image.load('entity.png')

    themap = []
    for j in range(16):
        row = []
        for i in range(16):
            row += [random.randint(0,1)]
        themap += [row]

    allimages = []
    
    for j in range(2):
        images = []
        image_size = 128
        for i in range(8):
            images += [textures[j].image_at((i*image_size, 0, image_size, image_size), colorkey=(255,255,255))]
            images[i].set_colorkey((0,0,0))
        allimages += [images]

    tiles = []
    for i in range(-9, 10):
        for j in range(-9, 10):
            tiles += [Tile(i, j)]
    
    entities = []
    for i in range(100):
        entities += [Entity(random.randint(-5,5), random.randint(-5,5))]
 
     

    rotation = 0
 
    # Loop until the user clicks the close button.
    done = False
 
    # Used to manage how fast the screen updates
    clock = pygame.time.Clock()
 
    # -------- Main Program Loop -----------
    while not done:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
 
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    ScrollXSpeed = -SCROLLSPEED
                if event.key == pygame.K_RIGHT:
                    ScrollXSpeed = SCROLLSPEED
                if event.key == pygame.K_UP:
                    ScrollYSpeed = SCROLLSPEED
                if event.key == pygame.K_DOWN:
                    ScrollYSpeed = -SCROLLSPEED
                if event.key == pygame.K_q:
                    movement = movement_speed
                if event.key == pygame.K_e:
                    movement = -movement_speed
 
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT:
                    ScrollXSpeed = 0
                if event.key == pygame.K_RIGHT:
                    ScrollXSpeed = 0
                if event.key == pygame.K_UP:
                    ScrollYSpeed = 0
                if event.key == pygame.K_DOWN:
                    ScrollYSpeed = 0
                if event.key == pygame.K_q:
                    movement = 0
                if event.key == pygame.K_e:
                    movement = 0
 
        ScrollY += ScrollYSpeed
        ScrollX += ScrollXSpeed
 

        for i in tiles:
            i.update(frame, rotation, ScrollX, ScrollY)
        for i in entities:
            i.update(frame, rotation, ScrollX, ScrollY)
        
        # ALL CODE TO DRAW SHOULD GO BELOW THIS COMMENT
        screen.fill((0,0,0))
        sorted_tiles = tiles
        sorted_tiles.sort(key=lambda x: x.y)
        for i in sorted_tiles:
            screen.blit(allimages[themap[i.gridx][i.gridy]][math.floor((rotation % 90) / 11.25)], (i.x, i.y))

        for i in entities:
            screen.blit(playerimage, (i.x, i.y))
        # ALL CODE TO DRAW SHOULD GO ABOVE THIS COMMENT
 
        clock.tick(60)
        frame += 1
        rotation += movement

        pygame.display.flip()
 

    pygame.quit()
 
if __name__ == "__main__":
    main()
