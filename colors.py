import pygame

image = pygame.image.load('texturepalette.png')

colors = []

for y in range(8):
    row = []
    for x in range(8):
        row += image[x][y]
        
    colors += row

print(colors)
